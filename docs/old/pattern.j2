
<h1>Patterns</h1>
<hr/>

<p>A pattern represents an ordered series of {{ doc('expr_intro', 'Expressions') }} that evaluate into notes or chords when used inside a {{ doc('clip') }}.  The basic idea is that when
describing patterns of notes in Warp, you just type in simple text expressions. Warp then evaluates these and they become notes.
</p>

<p>
Expressions can be very simple lists of notes or chords, or they can also include more complex modifiers.  When starting
out, just learn to type in notes and chords -- once you get those working, you can try {{ doc('expr_mod','Modifier Expressions') }} and then {{ doc('transform', 'Transforms') }}, which
are basically a kind of MIDI effect.</p>
</p>

<p>
Patterns have a <i>direction</i>, which is the order that the notes play. The default is <i>forward</i> - but there are also randomized directions of various kinds, including support
for serialism, a randomized composition technique.
</p>

<p>
Patterns may also define a tempo multiplier, called <i>rate</i>.  By default, the system is assumed to expect 16th notes, so to enter in 8th notes, set the rate to <i>0.5</i>, or for 32nd notes, <i>2</i>.
However, the system may contain any fractional rate, and all patterns may move at different speeds from one another.  Whether this is musically useful or not is up to your interpretation, but it is
quite possible.  The pattern rate is used in conjunction with the {{ doc('scene') }} rate and the {{ doc('clip') }} rate, so basically they are all multiplied together.
<p>

<p>
A pattern may optionally define a {{ doc('scale') }} to override the scale of the {{ doc('scene') }} or {{ doc('song') }}, which is used if not overridden by the {{ doc('clip') }}.</li>
</p>

<hr/>
<h2>A Quick Example</h2>

Patterns will make more sense if we show some examples. The simplest possible pattern is a scale:

{{ begin_code() }}1
2
3
4
5
6
7
8
{{ end_code() }}

Here are some major and minor chords in a pattern:

{{ begin_code() }}I
IV
V
i
iv
v
{{ end_code() }}

Here is a pattern some ties ("-") and rests (" "):

{{ begin_code() }}1
2
3
-

1
-
2
-
3
-

{{ end_code() }}

As mentioned, we could have also used literal notes names, which ignore the scale settings:

{{ begin_code() }}C4
D4
F6
Eb5
F#7
F0
A3
{{ end_code() }}

Patterns can be MUCH more complex and feature rich. For more, see the {{ doc('expr_intro', 'Expressions') }} chapter.

<hr/>
<h2>Directions</h2>

Patterns can have one of the following directions:

{{ begin_code() }}
forward
reverse
oscillate
pendulum
random
serialized
brownian1
brownian2
brownian3
brownian4
{{ end_code() }}

<ul>
<li>
<b>forward</b> - the pattern plays in a forward direction until the end of the pattern, or until <i>length</i> is reached.
If <i>length</i> is longer than the pattern, it will loop back around from the start.
</li>

<li>
<b>reverse</b> - this works exactly like forwards, except the pattern goes backwards starting from the end
</li>

<li>
<b>oscillate</b> - the pattern plays forward, and then in reverse, each time it repeats
</li>

<li>
<b>pendulum</b> - this works just like <i>oscillate</i>, but does not repeat the notes on the end of the pattern.
This usually sounds better than oscillate.
</li>

<li>
<b>random</b> - each time a note should play, a random value is selected from the list of slots.  These can
repeat.
</li>

<li>
<b>serialized</b> - this is like <i>random</i> but each value from <i>slots</i> must be used up before any more
random notes can be selected.  For instance, this could be used to play all the notes in a scale, but in any
random order, with each note being played exactly once.
</li>

<li>
<b>brownian1</b> - the first of six "brownian motion" modes which are slightly different from each other. In each of these, an index is chosen randomly
and then the next note will be to the left or right of this note in the <i>slots</i> list. The notes basically
bounce left or right in the pattern. If the note would bounce off the edge of the list it will wrap around and
pick the value on the other end. <i>brownian1</i> has the least chance of repeated notes of all of the six brownian modes.
</li>

<li>
<b>brownian2</b> is the same as <i>brownian1</i> but there is an equal chance the note moves left or right or the note is repeated - 33 1/3% chance each.
</li>

<li>
<b>brownian3</b> is the same as <i>brownian1</i> but the wrap-around behavior does not occur, modelling particles
bouncing against a glass on both sides.
</li>

<li>
<b>brownian4</b> is the same as <i>brownian2</i> without the wrap-around behavior.
</li>

<li>
<b>brownian5</b> is the same as <i>brownian1</i> but there is only a wall on the left.
</li>

<li>
<b>brownian6</b> is the same as <i>brownian2</i> but there is only a wall on the left.
</li>
</ul>

<li>
<b>build</b> plays through the pattern in order and then keeps repeating the very last note.  This isn't so useful for melodies but could be used
for great effect with {{ doc('expr_data_patterns','Data Patterns') }}, for instance, to slowly build a velocity and then keep it maximized, to construct a... you guessed it, build
of some kind.
</li>

{{ begin_info_block() }}
Pattern direction is shown in the <A HREF="https://bitbucket.org/laserllama/warpseq/src/master/examples/api/08_direction.py">08_direction.py</A> API source code demo.
{{ end_info_block() }}


{{ begin_info_block() }}
This will be even more powerful in the near future, as eventually {{ doc('expr_mod','Mod Expressions') }} will be able to change the direction dynamically, including based on probabilistic events, {{ doc('expr_variables','Variables') }}
set on other tracks, and {{ doc('expr_random','Randomness') }}.
{{ end_info_block() }}


<hr/>
<h2>Length</h2>

<p>
If not specified, the pattern contains the number of slots that you put into it. If a length <i>is</i> provided, however, and that number is shorter than the number
of notes in the pattern, only those first few notes will be used.
</p>

<p>
If the length of a pattern is set to be longer than the number of slots specified in the pattern, the pattern will wrap around, so for instance, if a pattern has 7 slots entered:

{{ begin_code() }}
1
2
3
4
5
6
7
{{ end_code() }}

... but the length was set to 10, when the pattern plays you would get:

{{ begin_code() }}
1
2
3
4
5
6
7
1
2
3
{{ end_code() }}

{{ begin_info_block() }}
In the near future, {{ doc('expr_mod','Mod Expressions') }} will be able to tweak the pattern length dynamically, including from variables {{ doc('expr_variables','Variables') }} set on
other tracks.
{{ end_info_block() }}



