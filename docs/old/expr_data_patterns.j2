<h1>Patterns As Data Sources</h1>
<hr/>

<p>
What follows is perhaps one of the most powerful features in Warp. It is somewhat analogous to {{ doc('expr_track','Track Grabs') }}
but much more open ended.
</p>

<p>To set the stage, assume you have written an amazing melody or minimal techno jam going, and you wouldn't mind hearing it loop around a bit.  But as you loop it
around and around, it would be great if you could introduce some variation. Now, yes, you could use {{ doc('expr_transforms','Transforms') }}, but
what if you want to do something a bit more intentional? This is when you ask yourself - What would John Sebastian Bach do?  To be honest, I have
no idea. But here is an example of what we did:
</p>

<p>Assume the following sequence in pattern called "melody1":</p>

{{ begin_code() }}
1
2
3
@foo
4
5
6
{{ end_code() }}

Each time the pattern would play the 4th note, what would happen is it will substitute <i>and consume</i> the next value to be produced
by the pattern named <i>foo</i>.  This is huge.

For instance, let's assume the pattern foo was this:

{{ begin_code() }}
4
5
6
{{ end_code() }}

If the pattern "melody1" were set to repeat 3 times, it would play the following notes:

{{ begin_code() }}
1
2
3
4 # < -- LOOK
4
5
6
{{ end_code() }}

then:

{{ begin_code() }}
1
2
3
5 # <-- LOOK
4
5
6
{{ end_code() }}

then:

{{ begin_code() }}
1
2
3
6 # <-- LOOK
5
6
{{ end_code() }}

It should be worth noting that the "@foo" syntax above is the same as this:

{{ begin_code() }}
1
2
3
CO n=@foo
4
5
6
{{ end_code() }}

This is because this feature is implemented internally as a shorthand for the "n=" {{ doc("expr_mod","Mod Expression") }} as
described in that chapter. Use the simpler syntax in most cases, though, it's shorter and more obvious.  However, it should
be noted that a probabilistic note grab is possible with the expanded syntax!

{{ begin_code() }}
1
2
3
7 p=0.25 n=@foo
4
5
6
{{ end_code() }}

In this example, there is a 25% chance that instead of playing the 7th note of the scale in the 4th slot, it will instead
grab the next note from the pattern "foo".


{{ begin_info_block() }}
Use of these Data Pattern</i> techniques can be used to construct mostly-repetive sequences with subtle variations. As such
they can serve as alternative to randomization techniques detailed in {{ doc('expr_random','Randomness') }} when the source
data pattern is set to a randomized direction, the types of which are listed in {{ doc('pattern','Patterns') }}
{{ end_info_block() }}

<hr/>
<h2>Drawing from Multiple Places</h2>

<p>
You could have a melody that you like and insert notes in different places from DIFFERENT source patterns, like so:
</p>

{{ begin_code() }}
1 ch=power
4
-
5
2
@foo
5 ch=power
6
7
@bar ch=major
2
3
4
@bar
-
-
{{ end_code() }}

<p>In this example, we are pulling note data from multiple patterns, and in some cases, using those to form chords. Remember
those patterns 'foo' and 'bar' could have been set to run in any direction, which makes this very powerful for creating evolving
patterns with very little effort. Wow.</p>


<hr/>
<h2>Skipping Implications</h2>

If one pattern samples a value from another pattern, and that also pattern is also one that is assigned to a clip (or maybe even
playing!) that will have the effect of that note being skipped in that other pattern.  This could be quite musically interesting,
depending on how it is used. But it also bears another point we'd like to stress: to use data patterns, the pattern does not
have to be one that is assigned to a clip!

<hr/>
<h2>Not Just For Notes</h2>

<p>
The data grab syntax used above can also be used in the parameters for any mod expression, for instance, imagine
this sequence pattern called "melody2"":
</p>

{{ begin_code() }}
I
IV v=@velocityPattern
V
{{ end_code() }}

<p>In the above example, the third chord in the sequence would grab a numeric velocity value from a pool. If that "velocityPattern" were c
configured as follows, this could produce chords that get quieter and quieter as the pattern repeats:</p>

{{ begin_code() }}
120
110
100
90
80
70
{{ end_code() }}

A better example might be drawing from a chord progression table, assuming we have a pattern called "chordChoices":

{{ begin_code() }}
major
minor
power
M6
m6
{{ end_code() }}

We could have a musical pattern (assigned to a clip), like this:

{{ begin_code() }}
1 ch=@chordChoices
2
3
4 ch=@chordChoices
5
6
7
{{ end_code() }}

In this example, the pattern will play a scale, but each time it hits the 1st and 4th note, it will play a chord instead,
drawing that chord type from the "chordChoices" pattern. The "chordChoices" pattern could be set to a forward direction, but could
also be set to many other random types, as described in {{ doc('pattern','Pattern') }}.

<h2>A Hack: Implementing a Skip!</h2>

While not the intended feature of this construction, the tools above allow for some emergent behavior.  We can implement a probabilistic skip over a track in the current pattern by
referencing the pattern name inside the pattern itself. Assume a pattern named "melody3":

{{ begin_code() }}
1
2
3 p=0.25 n=@melody3
4
5
6
{{ end_code() }}

<p>
In this example, the system has a chance of skipping over "3" and going straight to 4.
</p>

<h2>Things To Be Aware Of</h2>

Data Patterns are not musical patterns, so you cannot put {{ doc('expr_mod','Mod Expressions') }} like <i>p=0.25 shuffle</i> or <i>$x+2</i> on a step in a data pattern.  You can of course set their
pattern direction to implement the first thing.

<h2>Learning In Context</h2>

<p>
This is one of the most advanced features in Warp, so all of the above may feel a little abstract. The example <A HREF="https://bitbucket.org/laserllama/warpseq/src/master/examples/api/09_data_patterns.py">09_data_patterns.py</A> shows
how pattern grabs are used in context, both sampling notes from other patterns and also sampling velocity data. Keep in mind that ANY parameter to any {{ doc('expr_mod','Mod Expression') }} can
be used with "@", so feel free to get creative!
</p>

<p>
Try and experiment, and patterns drawing data from other patterns can help breathe life into otherwise stagnant repitition, or provide ways to quickly apply chord progressions from tables to specific notes in rotation.
</p>

{{ begin_info_block() }}
Tip: in most cases, using these kinds of pattern draws will sound much more interesting if you allow your patterns to repeat more than once.
{{ end_info_block() }}


