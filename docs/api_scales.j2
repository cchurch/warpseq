<h1>Scales</h1>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
<h2>About</h2>

<p>
Warp natively understands musical scales/modes, so that notes in {{ doc('api_patterns','Patterns') }} can be expressed as scale degrees
and easily shifted between different scales, without having to rely on classic 'force to scale' mechanics that do not
provide support for blue notes. In Warp, scales can either be generated from included <i>scale types</i>, or user supplied,
by expressing them in scale degrees.
</p>

<p>
While a default scale can be assigned to a {{ doc('api_song', 'Song') }}, scale
assignments can be overriden on individual {{ doc('api_scenes', 'Scenes') }} or {{ doc('api_clips','Clips') }}.
</p>

<p>
As it's important to support percussion tracks, it's possible to enter literal notes such as "C0" without using
scale degrees or being subject to scale transpositions.  We'll showcase this when we get to {{ doc('api_slots','Slots') }}.
</p>

<!--------------------------------------------------------------------------------------------------------------------->

<hr/>
{{ table_of_contents() }}

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Creating A Scale") }}

<p>
Canned scales:
</p>

{{ begin_code(language='python') }}
api.scales.add(name='intro', note='C', octave=0, scale_type='major')
{{ end_code() }}

<p>
User defined scale types:
</p>

{{ begin_code(language='python') }}
api.scales.add(name='user_example', note='C', octave=0, slots=[ 1, 2, 'b3', 5, 'b6' ])
{{ end_code() }}

<p>
A scale has the following properties:
</p>

<table border=1 cellpadding="10px">
<tr>
  <td>Property</td>
  <td>Type</td>
  <td>Description</td>
</tr>
<tr>
  <td>name</td>
  <td>string</td>
  <td>A symbolic name for the scale, which will can be refererenced from a {{ doc('api_song','Song') }}, {{ doc('api_scenes','Scene') }},
  or {{ doc('api_clips','Clip') }}.</td>
<tr>
  <td>note</td>
  <td>string</td>
  <td>The root of the scale, such as "C", "Bb" or "F#"</td>
</tr>
<tr>
  <td>octave</td>
  <td>int</td>
  <td>the base octave - note that transpositions below this value are not possible, so it is better to adjust
  the global octave using the parameter <i>base_octave</i> on {{ doc('api_instruments','Instrument') }}.</td>
</tr>
<tr>
  <td>scale_type</td>
  <td>string</td>
  <td>the name of a built-in scale type to construct the scale from - see below for values</td>
<tr>
  <td>slots</td>
  <td>list of ints/strings</td>
  <td>The scale degrees to be used in forming the scale as shown in the above example. Mutually exclusive with
  <i>scale_type</i>.</td>
</tr>
</table>

{{ begin_info_block() }}
Tip: naming your scales things like <i>intro</i> is better than naming them <i>Eb-natural-minor</i> because
you can decide to change the definition of <i>intro</i> later to try out large-scale variations in your
song, anywhere <i>intro</i> is referenced.
{{ end_info_block() }}

<!--------------------------------------------------------------------------------------------------------------------->

<hr/>
{{ section("Built-In Scales") }}

Valid values for <i>scale_type</i> are:

{{ begin_code(language='python') }}
   major              = [ 1, 2, 3, 4, 5, 6, 7 ],
   pentatonic         = [ 1, 2, 3, 5, 6 ],
   pentatonic_minor   = [ 1, 3, 4, 5, 7 ],
   natural_minor      = [ 1, 2, 'b3', 4, 5, 'b6', 'b7' ],
   blues              = [ 1, 'b3', 4, 'b5', 5, 'b7' ],
   dorian             = [ 1, 2, 'b3', 4, 5, 6, 'b7' ],
   chromatic          = [ 1, 'b2', 2, 'b3', 3, 4, 'b5', 5, 'b6', 6, 'b7', 7 ],
   harmonic_major     = [ 1, 2, 3, 4, 5, 'b6', 7 ],
   harmonic_minor     = [ 1, 2, 3, 4, 5, 'b6', 7 ],
   locrian            = [ 1, 'b2', 'b3', 4, 'b5', 'b6', 'b7' ],
   lydian             = [ 1, 2, 3, 'b5', 5, 6, 7 ],
   major_pentatonic   = [ 1, 2, 3, 5, 6 ],
   melodic_minor_asc  = [ 1, 2, 'b3', 4, 5, 'b7', 'b8', 8 ],
   melodic_minor_desc = [ 1, 2, 'b3', 4, 5, 'b6', 'b7', 8 ],
   minor_pentatonic   = [ 1, 'b3', 4, 5, 'b7' ],
   mixolydian         = [ 1, 2, 3, 4, 5, 6, 'b7' ],
   phrygian           = [ 1, 'b2', 'b3', 4, 5, 'b6', 'b7' ],
   japanese           = [ 1, 2, 4, 5, 6 ],
   akebono            = [ 1, 2, 'b3', 5, 6 ]
{{ end_code() }}

<p>
This list will grow over time.  {{ doc('contributing','Contributions') }} of your favorite scales are welcome.
</p>

<!--------------------------------------------------------------------------------------------------------------------->

<hr/>
{{ section("Suggestions & Tips") }}

<ul>
<li>If you don't set a scale anywhere, you'll get the C chromatic scale, and that's probably not going to sound
like you want.</li>
</ul>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("In Context") }}

Reading the following API examples is recommended for understanding this section:

<ul>
<li><A HREF="https://bitbucket.org/laserllama/warpseq/src/master/examples/api/01_scales.py">Scales</A>
</ul>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Next Up") }}

See {{ doc('api_tracks','Patterns') }}